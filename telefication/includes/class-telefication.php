<?php

/**
 * The file that defines the core plugin class
 *
 * @link       https://telefication.ir/wordpress-plugin
 * @since      1.0.0
 *
 * @package    Telefication
 * @subpackage Telefication/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Telefication
 * @subpackage Telefication/includes
 * @author     Foad Tahmasebi <tahmasebi.f@gmail.com>
 */
class Telefication {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Telefication_Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Options of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array $options Options of the plugin from database.
	 */
	protected $options;

	/**
	 * Post id to sent to channel
	 *
	 * @since 1.7.0
	 * @access   protected
	 * @var
	 */
	protected $post_id;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		if ( defined( 'TELEFICATION_VERSION' ) ) {
			$this->version = TELEFICATION_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'telefication';
		$this->options     = get_option( 'telefication' );

		$this->load_dependencies();
		$this->set_locale();
		$this->core_functionality();

		if ( is_admin() ) {
			$this->define_admin_hooks();
		}

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Telefication_Loader. Orchestrates the hooks of the plugin.
	 * - Telefication_i18n. Defines internationalization functionality.
	 * - Telefication_Admin. Defines all hooks for the admin area.
	 *
	 * Create an instance of the loader which will be used to register the hooks with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telefication-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Html2TextException.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/Html2Text.php';

		/**
		 * The class responsible for defining internationalization functionality of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telefication-i18n.php';

		/**
		 * The class responsible for communicate with Telefication server.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-telefication-service.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		if ( is_admin() ) {
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-telefication-admin.php';
		}

		$this->loader = new Telefication_Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Telefication_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Telefication_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Add hooks according to the settings.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function core_functionality() {

		// Notify for email - Filter
		if ( isset( $this->options['email_notification'] ) && '1' == $this->options['email_notification'] ) {
			$this->loader->add_filter( 'wp_mail', $this, 'telefication_filter_wp_mail' );
		}

		// New order action
		if ( isset( $this->options['is_woocommerce_only'] ) && '1' == $this->options['is_woocommerce_only'] ) {
			// if woocommerce is active
			$this->loader->add_action( 'woocommerce_order_status_changed', $this, 'telefication_action_woocommerce_order_status_changed', 10, 4 );
		}

		// New comment action
		if ( isset( $this->options['new_comment_notification'] ) && '1' == $this->options['new_comment_notification'] ) {

			$this->loader->add_action( 'wp_insert_comment', $this, 'telefication_action_wp_insert_comment' );
		}

		// New post action
		if ( isset( $this->options['new_post_notification'] ) && '1' == $this->options['new_post_notification'] ) {

			$this->loader->add_action( 'transition_post_status', $this, 'telefication_action_publish_post', 10, 3 );
		}

		// New post action for Send to channel
		if ( isset( $this->options['send_to_channel_enable'] ) && '1' == $this->options['send_to_channel_enable'] &&
		     isset( $this->options['bot_token'] ) && ! empty( $this->options['bot_token'] ) ) {

			$this->loader->add_action( 'transition_post_status', $this, 'telefication_action_publish_post_send_to_channel', 10, 3 );
		}

		// New user action
		if ( isset( $this->options['new_user_notification'] ) && '1' == $this->options['new_user_notification'] ) {

			$this->loader->add_action( 'user_register', $this, 'telefication_action_user_register', 10 );
		}
		// Daily reminder action
		$this->loader->add_action('woocommerce_no_stock_notification', $this, 'telefication_out_of_stock', 10);
		$this->loader->add_action('woocommerce_low_stock_notification', $this, 'telefication_low_in_stock', 10);
		$this->loader->add_action('woocommerce_product_on_backorder_notification', $this, 'telefication_on_backorder_stock', 10);
		$this->loader->add_action('end_day_order_summary', $this, 'telefication_end_day_order_summary', 10);
		$this->loader->add_action('delivery_reminder_evening', $this, 'telefication_delivery_reminder_evening', 10);
		$this->loader->add_action('delivery_reminder_morning', $this, 'telefication_delivery_reminder_morning', 10);
		// $this->loader->add_action('leaderboard_summary', $this, 'telefication_leaderboard_summary', 10);
	}

	/**
	 * Leaderboard Alternative
	 */
	// public function telefication_leaderboard_summary(){
	// 	$top_seller = new WC_SRE_Row_Top_Sellers('2021-04-11');
	// 	$top_seller->get_top_sellers_only();
	// 	error_log(print_r('Im doneee', true));
	// }
	/**
	 * 
	 * Notifbot Error
	 */
	public function telefication_notifbot_invalid_key(){
		$message = "Key is invalid and could not send Whatsapp message to user in system";
		$telefication_service = new Telefication_Service( $this->options );
		if ($telefication_service->create_url( $message )){
			$telefication_service->send_notification();
		}
	}
	/**
	 * Stock Status Reminder
	 * 
	 */
	public function telefication_out_of_stock ( $array ){
		$message = "⛔️⛔️ ". print_r($array->get_name(), true) . " is <b>Out of Stock</b> | Qty : " . print_r($array->get_stock_quantity(), true) . " ⛔️⛔️";
		$telefication_service = new Telefication_Service( $this->options );
		if ($telefication_service->create_url( $message )){
			$telefication_service->send_notification();
		}
	}
	
	public function telefication_low_in_stock ( $product ){
		$message = "⚠️⚠️ ".$product->get_formatted_name(). " is <b>Low in stock</b> | Qty : " .$product->get_stock_quantity(). " ⚠️⚠️";
		$telefication_service = new Telefication_Service( $this->options );
		if ($telefication_service->create_url( $message )){
			$telefication_service->send_notification();
		}
	}

	public function telefication_on_backorder_stock ( $array ){
		$message = "🛑🛑 ".$array['product']->get_name(). " is <b>Stock is in Backorder</b> | Qty : " .$array['product']->get_stock_quantity(). " 🛑🛑";
		$telefication_service = new Telefication_Service( $this->options );
		if ($telefication_service->create_url( $message )){
			$telefication_service->send_notification();
		}
	}

	/**
	 * Delivery Reminder
	 */
	public function telefication_delivery_reminder_morning(){
		if ( get_transient( 'telefication_delivery_reminder_morning' ) ) return;
		set_transient( 'telefication_delivery_reminder_morning', true, 60*60*12 ); //5 jam transient
		$args = array(
			'limit'    => -1,
			'status'   => array('processing','pending'),
			'meta_key' => '_delivery_date',
			'meta_value' => Date('Y-m-d'),
			'meta_compare' => 'EQUAL'
		);
		$order_wc = wc_get_orders($args);
		$session = '';
		$message = '';
		if (date('H') + 8 < 10){
			$session = 'morning';
			$message = "🚚 Delivery at 10:00am 🚚\n";
			$message .= "===================\n\n" ;
		}elseif (date('H') + 8 < 20 && 10 < date('H') + 8){
			$session = 'evening';
			$message = "🚚 Delivery at 4:00pm 🚚\n";
			$message .= "=================\n\n";
		}

		$key_button = array();
		$count = 1;
		$has_order = false;
		foreach ($order_wc as $order){
			$del_time_from = explode(":", $order->get_meta('_delivery_time_frame', true)['time_from']);
			if ($del_time_from[0] < 10 && $session == 'morning'){
				$message .= $count . '. Order ' . $order->get_order_number() . "\n";
				array_push($key_button, array('text' => $count, 'callback_data' => 'order_'. $order->get_id()));
				$count++;
				$has_order = true;
			}elseif ($del_time_from[0] < 16 && 10 < $del_time_from[0] && $session == 'evening'){
				$message .= $count . '. Order ' . $order->get_order_number() . "\n";
				array_push($key_button, array('text' => $count, 'callback_data' => 'order_'. $order->get_id()));
				$count++;
				$has_order = true;
			}
		}
		$message .= "\nGet order item by selecting the number below";
		
		$keyboard = array(
			"inline_keyboard" => array(
				$key_button
			)
		);
		
		if ($has_order == false){
			$message = '';
			if ($session == 'evening'){
				$message .= 'No order to be delivered at 4:00pm ☹️'	;		
			}else{
				$message .= 'No order to be delivered at 10:00am ☹️';
			}
			$telefication_service = new Telefication_Service( $this->options );
			if ($telefication_service->create_url( $message )){
				$telefication_service->send_notification();
			}
		}else{
			$telefication_service = new Telefication_Service( $this->options );
			if ($telefication_service->create_url( $message, json_encode($keyboard, true) )){
				$telefication_service->send_notification();
			}
		}
	}
	
	public function telefication_delivery_reminder_evening(){
		if ( get_transient( 'telefication_delivery_reminder_evening' ) ) return;
		set_transient( 'telefication_delivery_reminder_evening', true, 60*60*12 ); //5 jam transient
		$args = array(
			'limit'    => -1,
			'status'   => array('processing','pending'),
			'meta_key' => '_delivery_date',
			'meta_value' => Date('Y-m-d'),
			'meta_compare' => 'EQUAL'
		);
		$order_wc = wc_get_orders($args);
		$session = '';
		$message = '';
		if (date('H') + 8 < 10){
			$session = 'morning';
			$message = "🚚 Delivery at 10:00am 🚚\n";
			$message .= "===================\n\n" ;
		}elseif (date('H') + 8 < 20 && 10 < date('H') + 8){
			$session = 'evening';
			$message = "🚚 Delivery at 4:00pm 🚚\n";
			$message .= "=================\n\n";
		}

		$key_button = array();
		$count = 1;
		$has_order = false;
		foreach ($order_wc as $order){
			$del_time_from = explode(":", $order->get_meta('_delivery_time_frame', true)['time_from']);
			if ($del_time_from[0] < 10 && $session == 'morning'){
				$message .= $count . '. Order ' . $order->get_order_number() . "\n";
				array_push($key_button, array('text' => $count, 'callback_data' => 'order_'. $order->get_id()));
				$count++;
				$has_order = true;
			}elseif ($del_time_from[0] < 16 && 10 < $del_time_from[0] && $session == 'evening'){
				$message .= $count . '. Order ' . $order->get_order_number() . "\n";
				array_push($key_button, array('text' => $count, 'callback_data' => 'order_'. $order->get_id()));
				$count++;
				$has_order = true;
			}
		}
		$message .= "\nGet order item by selecting the number below";
		
		$keyboard = array(
			"inline_keyboard" => array(
				$key_button
			)
		);
		
		if ($has_order == false){
			$message = '';
			if ($session == 'evening'){
				$message .= 'No order to be delivered at 4:00pm ☹️'	;		
			}else{
				$message .= 'No order to be delivered at 10:00am ☹️';
			}
			$telefication_service = new Telefication_Service( $this->options );
			if ($telefication_service->create_url( $message )){
				$telefication_service->send_notification();
			}
		}else{
			$telefication_service = new Telefication_Service( $this->options );
			if ($telefication_service->create_url( $message, json_encode($keyboard, true) )){
				$telefication_service->send_notification();
			}
		}
	}
	
	/**
	 * End Day Pending Payment Summary
	 * 
	 */
	public function telefication_end_day_order_summary(){
		if ( get_transient( 'telefication_end_day_order_summary' ) ) return;
		set_transient( 'telefication_end_day_order_summary', true, 60*60*12 ); //12 jam transient
		
		$message = "<b>End Day Report</b>\n";
		$message .= date("d.m.Y (l)") . "\n";
		$message .= "================\n\n";
		
		// get pending payment of the day
		$current_date = date("Y-m-d");
		$orders = wc_get_orders( array(
			'limit'    => -1,
			'status'   => 'pending',
			'date_created' => $current_date,
		));
		if (empty($orders)){
			$message .= "💵 <b>Tak Da Orang Hutang</b> 💵\n";
		}else{
			$message .= "💵💵 <b>Orang Hutang</b> 💵💵\n";
			$count = 0;
			foreach ( $orders as $order ) {
				$count++;
				$message .= $count . ". [" . $order->get_order_number() . "] " . $order->get_billing_first_name() . " " . $order->get_billing_last_name() . " ( RM " . $order->get_total() . " )\n";
			}
		}
		
		$telefication_service = new Telefication_Service( $this->options );
		if ( $telefication_service->create_url( $message ) ) {
			$telefication_service->send_notification();
		}
		sleep(10);
		$selected_category_slug = get_option('hca_category_telefication_category_filter', 10); // get category to exclude
		$low_stock_amount_default = get_option('woocommerce_notify_low_stock_amount', 10); // get woocommerce default threshold
		
		$args = array(
				'taxonomy' => 'product_cat',
				'orderby' => 'name',
				'order' => 'ASC',
				'hide_empty' => false,
				'parent' => 0,
		);
		$categories_unfiltered = new WP_Term_Query($args);
		$cat_slug_unfiltered = array();
		foreach ($categories_unfiltered->get_terms(array('include_children' => false)) as $category){
			array_push($cat_slug_unfiltered, $category->slug);
		}
		$product_cats = array_diff($cat_slug_unfiltered, $selected_category_slug);
		foreach  ($product_cats as $cat){
			$this->telefication_message_by_category($cat, $low_stock_amount_default);
			sleep(10);
		}
	}

	public function telefication_message_by_category( $category_slug, $low_stock_amount_default ){
		$category = get_term_by( 'slug', $category_slug, 'product_cat' );
		$message = $category->name . " Products Summary\n";
		$message .= "================\n\n";
		$out_stock_message = "";
		$low_stock_message = "⚠️⚠️ <b>Low Stock Item </b>⚠️⚠️\n";
		
		$params = array(
			'post_type' => 'product',
			'posts_per_page' => -1,
			'meta_query' => array(
				array('key' => '_stock',
							'value' => $low_stock_amount_default,
							'compare' => '<=',
				),
				array('key' => 'total_sales',
							'value' => 10,
							'compare' => '>',
				)
			),
			'meta_key'   => 'total_sales',
			'orderby'    => 'meta_value_num',
			'order'      => 'DECS',
			'tax_query' => array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'product_cat',
					'field' => 'slug',
					'terms' => $category_slug,
					'operator' => 'IN',
				)
			),
		);

		$products = new WP_Query($params);
		if( $products->have_posts() ) {
			$count_low = 0;
			$count_out = 0;
			$has_out_of_stock = false;
			$has_low_stock = false;
			while( $products->have_posts() ) {
				$products->the_post();
				global $product;
				$low_stock = ($product->get_low_stock_amount() == '') ? $low_stock_amount_default : $product->get_low_stock_amount();
				$stock = $product->get_stock_quantity();
				if ($stock <= $low_stock && $stock > 0 && $product->get_manage_stock() == 1){
					$has_low_stock = true;
					$count_low++;
					$low_stock_message .= $count_low . ". " . $product->get_name() . " | Qty : " . $stock . "\n";
				}elseif ($stock <= 0 && $product->get_manage_stock() == 1){
					$has_out_of_stock = true;
					$count_out++;
					$out_stock_message .= $count_out . ". " . $product->get_name() . " | Qty : " . $stock . "\n";
				}
			}
		}

		// Low Stock Items
		if ($has_low_stock){
			$message .= $low_stock_message;
		}else{
			$message .= "🟢🟢 <b>No Low Stock Item </b>🟢🟢\n";
		}
		// Out of Stock items
		if ($has_out_of_stock){
			$message .= "\n\n❗️❗️ <b>Out Of Stock</b> ❗️❗️\n";
			$message .= $out_stock_message;
		}else{
			$message .= "\n\n🟢🟢 <b>No Out Of Stock</b> 🟢🟢\n";
		}
		
		$telefication_service = new Telefication_Service( $this->options );
		if ( $telefication_service->create_url( $message ) ) {
			$telefication_service->send_notification();
		}
	}

	/**
	 * New User Registration Action
	 *
	 * @param $user_id
	 *
	 * @since 1.4.0
	 *
	 */
	public function telefication_action_user_register( $user_id ) {

		//notification body
		$message = get_bloginfo( 'name' ) . ":\n\n";
		$message .= __( 'New User Registered.', 'telefication' ) . "\n\n";

		$message .= site_url();

		$telefication_service = new Telefication_Service( $this->options );

		if ( $telefication_service->create_url( $message ) ) {
			$telefication_service->send_notification();
		}
	}

	/**
	 * New Post Publish Action
	 *
	 * @param $new_status
	 * @param $old_status
	 * @param $post
	 *
	 * @since 1.4.0
	 *
	 */
	public function telefication_action_publish_post( $new_status, $old_status, $post ) {

		// if new post published
		if ( 'publish' === $new_status && 'publish' !== $old_status ) {

			// is post type is post, for notification
			if ( $post->post_type === 'post' ) {

				//notification body
				$message = get_bloginfo( 'name' ) . ":\n\n";
				$message .= __( 'New Post: ', 'telefication' ) . "\n-----\n\n";
				$message .= $post->post_title . "\n\n";

				$message .= __( 'Post URL: ', 'telefication' ) . get_permalink( $post->ID );

				$telefication_service = new Telefication_Service( $this->options );

				if ( $telefication_service->create_url( $message ) ) {
					$telefication_service->send_notification();
				}
			}

		}

	}

	/**
	 * New Post Publish Action For Sending To Channel
	 *
	 * @param $new_status
	 * @param $old_status
	 * @param $post
	 *
	 * @since 1.7.0
	 *
	 */

	public function telefication_action_publish_post_send_to_channel( $new_status, $old_status, $post ) {
		// if new post published
		if ( 'publish' === $new_status && 'publish' !== $old_status ) {
			if ( isset( $this->options['channel_username'] ) && ! empty( $this->options['channel_username'] ) ) {
				if ( isset( $this->options['telefication_channel_post_type'] ) ) {

					$telefication_channel_post_type = $this->options['telefication_channel_post_type'];
					$post_type                      = $post->post_type;

					// if post type is allowed
					if ( array_key_exists( $post_type, $telefication_channel_post_type ) && $telefication_channel_post_type[ $post_type ] === "1" ) {
						$this->post_id = $post->ID;
						//add it to shutdown to send after saving meta data
						add_action( 'shutdown', array( $this, 'sendToChannel' ), 10 );
					}
				}
			}
		}
	}

	/**
	 * Send Post To Channel
	 *
	 * @since 1.7.0
	 */
	public function sendToChannel() {

		$post                 = get_post( $this->post_id );
		$channel_post         = $this->telefication_create_channel_post( $post );
		$telefication_service = new Telefication_Service( $this->options );
		$thumbnail_url        = get_the_post_thumbnail_url( $post );

		if ( isset( $this->options['channel_featured_image_enable'] ) &&
		     $this->options['channel_featured_image_enable'] == '1' &&
		     ! empty( $thumbnail_url )
		) {
			if ( $telefication_service->sendPhoto( $channel_post,
				$thumbnail_url,
				$this->options['channel_username'] ) ) {

				$telefication_service->send_notification();
			}

		} else {
			if ( $telefication_service->create_url( $channel_post,
				$this->options['channel_username'] ) ) {
				$telefication_service->send_notification();
			}
		}
	}

	/**
	 * Make channel post by template
	 *
	 * @param $post
	 *
	 * @return mixed|string
	 * @since 1.5.0
	 *
	 */
	public function telefication_create_channel_post( $post ) {

		$dictionary = array(
			'{title}'              => strip_tags( $post->post_title, '<b><i><a><code><pre><u><ins><s><strike><del>' ),
			'{content}'            => strip_tags( $post->post_content, '<b><i><a><code><pre><u><ins><s><strike><del>' ),
			'{excerpt}'            => strip_tags( $post->post_excerpt, '<b><i><a><code><pre><u><ins><s><strike><del>' ),
			'{post_link}'          => strip_tags( wp_get_shortlink( $post->ID ), '<b><i><a><code><pre><u><ins><s><strike><del>' ),
			'{post_category}'      => strip_tags( get_the_category( $post->ID )[0]->name, '<b><i><a><code><pre><u><ins><s><strike><del>' ),
			'{post_type}'          => strip_tags( $post->post_type, '<b><i><a><code><pre><u><ins><s><strike><del>' ),
			'{product_price}'      => '',
			'{product_sale_price}' => '',
			'{product_category}'   => '',
		);

		// product information
		if ( $post->post_type == 'product' ) {

			$product = wc_get_product( $post->ID );

			$dictionary['{product_price}']      = $product->get_regular_price();
			$dictionary['{product_sale_price}'] = $product->get_sale_price();
			$dictionary['{product_category}']   = strip_tags( get_the_terms( $post->ID, 'product_cat' )[0]->name, '<b><i><a><code><pre><u><ins><s><strike><del>' );
		}

		$message = '';
		if ( $post->post_type != 'product' ) {
			if ( isset( $this->options['channel_notification_template'] ) ) {
				$message = str_replace( array_keys( $dictionary ), $dictionary,
					urldecode( $this->options['channel_notification_template'] ) );
			}
		} else {
			if ( isset( $this->options['channel_woocommerce_template'] ) ) {
				$message = str_replace( array_keys( $dictionary ), $dictionary,
					urldecode( $this->options['channel_woocommerce_template'] ) );
			}
		}

		return $message;
	}

	/**
	 * New Comment Action
	 *
	 * @param $comment_ID
	 *
	 * @return bool
	 * @since 1.4.0
	 *
	 */
	public function telefication_action_wp_insert_comment( $comment_ID ) {

		$status = wp_get_comment_status( $comment_ID );

		if ( $status == 'spam' ) {
			return false;
		}

		$comment_text = get_comment_text( $comment_ID );

		//notification body
		$message = get_bloginfo( 'name' ) . ":\n\n";
		$message .= __( 'New Comment: ', 'telefication' ) . "\n-----\n\n";
		$message .= $comment_text . "\n\n";

		$message .= __( 'Comment Link: ', 'telefication' ) . get_comment_link( $comment_ID );

		$telefication_service = new Telefication_Service( $this->options );

		if ( $telefication_service->create_url( $message ) ) {
			$telefication_service->send_notification();
		}
	}

	/**
	 * Add filter callback function for 'wp_email'
	 *
	 * Get email subject, generate notification body and send notification
	 *
	 * @param  array  $email_args
	 *
	 * @return array $email_args
	 * @throws Html2TextException
	 * @since    1.0.0
	 * @access   public
	 */
	public function telefication_filter_wp_mail( $email_args ) {

		//check for if notification is not allowed for this recipient.
		if ( isset( $this->options['match_emails'] ) && ! empty( $this->options['match_emails'] ) ) {
			$emails = explode( ',', $this->options['match_emails'] );

			if ( ! in_array( $email_args['to'], $emails ) ) {
				return $email_args;
			}
		}

		// check for adding recipient email
		$to = ( isset( $this->options['display_recipient_email'] ) && '1' == $this->options['display_recipient_email'] ) ? $email_args['to'] : '';

		$message = get_bloginfo( 'name' ) . ": " . $to . "\n\n";
		$message .= $email_args['subject'] . "\n\n";

		// check for adding email body option
		if ( isset( $this->options['send_email_body'] ) && '1' == $this->options['send_email_body'] ) {
			$message .= Html2Text::convert( $email_args['message'], true ) . "\n\n";
		}

		$message .= site_url();

		$telefication_service = new Telefication_Service( $this->options );

		if ( $telefication_service->create_url( $message ) ) {
			$telefication_service->send_notification();
		}

		return $email_args;
	}

	/**
	 * Add action callback function for 'woocommerce_thankyou'
	 *
	 * Get order data, generate notification body and send notification for woocommerce new order
	 *
	 * @param $order_id
	 * @param $status_from
	 * @param $status_to
	 * @param $order
	 *
	 * @return bool
	 * @since    1.0.0
	 * @access   public
	 */

	public function telefication_action_woocommerce_order_status_changed( $order_id, $status_from, $status_to, $order ) {
		if (metadata_exists('post', $order_id, '_op_order_source') == true) {
			return true;
		}

		if ( ! isset( $this->options[ 'notify_' . $status_to . '_order' ] ) ||
		     '1' != $this->options[ 'notify_' . $status_to . '_order' ] ) {
			return true;
		}

		// Get an instance of the WC_Order object
		$order_data = $order->get_data();

		$shipping_info = '';
		if ( isset( $this->options['include_shipping_info'] ) && $this->options['include_shipping_info'] == '1' ) {

			$shipping_info .= __( 'Shipping Info:', 'telefication' ) . " \n-----\n";

			$shipping_info .= __( 'Name: ', 'telefication' ) . $order_data['shipping']['first_name'] . " " . $order_data['shipping']['last_name'] . "\n";
			$shipping_info .= __( 'Country: ', 'telefication' ) . $order_data['shipping']['country'] . "\n";
			$shipping_info .= __( 'City: ', 'telefication' ) . $order_data['shipping']['city'] . "\n";

			$shipping_info .= __( 'Postcode: ', 'telefication' ) . $order_data['shipping']['postcode'] . "\n";
			$shipping_info .= __( 'State: ', 'telefication' ) . $order_data['shipping']['state'] . "\n";

			$shipping_info .= __( 'Address 1: ', 'telefication' ) . $order_data['shipping']['address_1'] . "\n";
			$shipping_info .= __( 'Address 2: ', 'telefication' ) . $order_data['shipping']['address_2'] . "\n\n";
		}

		$billing_info = '';
		if ( isset( $this->options['include_billing_info'] ) && $this->options['include_billing_info'] == '1' ) {

			$billing_info .= __( 'Billing Info:', 'telefication' ) . " \n-----\n";

			$billing_info .= __( 'Name: ', 'telefication' ) . $order_data['billing']['first_name'] . " " . $order_data['billing']['last_name'] . "\n";

			$billing_info .= __( 'Email: ', 'telefication' ) . $order_data['billing']['email'] . "\n";
			$billing_info .= __( 'Phone: ', 'telefication' ) . $order_data['billing']['phone'] . "\n";

			$billing_info .= __( 'Country: ', 'telefication' ) . $order_data['billing']['country'] . "\n";
			$billing_info .= __( 'City: ', 'telefication' ) . $order_data['billing']['city'] . "\n";

			$billing_info .= __( 'Postcode: ', 'telefication' ) . $order_data['billing']['postcode'] . "\n";
			$billing_info .= __( 'State: ', 'telefication' ) . $order_data['billing']['state'] . "\n";

			$billing_info .= __( 'Address 1: ', 'telefication' ) . $order_data['billing']['address_1'] . "\n";
			$billing_info .= __( 'Address 2: ', 'telefication' ) . $order_data['billing']['address_2'] . "\n\n";
		}


		$items = ""; // items_detail
		if ( isset( $this->options['include_items_detail'] ) && $this->options['include_items_detail'] == '1' ) {

			foreach ( $order->get_items() as $item_key => $item_values ) {

				$item_data = $item_values->get_data();

				$product_name = $item_data['name'];
				$quantity     = $item_data['quantity'];

				$items .= "$product_name * $quantity \n";
			}
			$items .= "\n";
		}
		$current_date = date('Y-m-d');
		$order_date = date('Y-m-d', strtotime($order->get_meta('_delivery_date')));
		$delivery_date = date_diff(date_create($current_date), date_create($order_date))->days == '0' ? '<b>Today</b>' : date("d-m-Y", strtotime($order_date));
		
		$message .= __( '🏃🏻‍♂️ New web online order ', 'telefication' ) . $order_id .' (RM '.$order_data['total'].') '. 'is in! 🏃🏻‍♂️';
		$message .= "\n<b>Delivery Date & Time </b>: " . $delivery_date . ' at ' . $order->get_meta('_delivery_time_frame')['time_from'] . ' - ' . $order->get_meta('_delivery_time_frame')['time_to'] . "\n";
		
		$message .= $items;
		$message .= $shipping_info;
		$message .= $billing_info;

		// custom inline keyboard button - need custom call function and argument
		$keyboard = array(
			"inline_keyboard" => array(
				array(
					array(
						"text" => "Get Item 📝", "callback_data" => "order_". $order_id
					),
					array(
						"text" => "View Order 🔗", "url" => site_url() . '/wp-admin/post.php?post=' . $order_id . '&action=edit'
					)
				)
			)
		);

		$telefication_service = new Telefication_Service( $this->options );
		if ( $telefication_service->create_url( $message, json_encode($keyboard, true) ) ) {
			$telefication_service->send_notification();
		}
	}

	/**
	 * Register all of the hooks related to the admin area functionality of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Telefication_Admin( $this->plugin_name, $this->version, $this->options );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_telefication_page' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'init_telefication_page' );

		// Since 1.3.0
		$this->loader->add_action( 'wp_ajax_send_telefication_test_message', $this, 'send_test_message' );
		$this->loader->add_action( 'wp_ajax_get_telefication_chat_id', $this, 'get_chat_id' );

		// add link of Telefication setting page in plugins page
		$this->loader->add_filter( 'plugin_action_links_' . TELEFICATION_BASENAME, $plugin_admin, 'add_action_links' );
	}

	/**
	 * Ajax sent test message
	 *
	 * @since    1.3.0
	 */
	public function send_test_message() {

		$message = ( isset( $_REQUEST['message'] ) ) ? $_REQUEST['message'] : __( 'This Is Test', 'telefication' );

		if ( ! isset( $_REQUEST['chat_id'] ) || empty( $_REQUEST['chat_id'] ) ) {
			_e( 'Please enter ID', 'telefication' );
			die;
		}

		$telefication_service          = new Telefication_Service( $this->options );
		$telefication_service->chat_id = $_REQUEST['chat_id'];

		if ( $telefication_service->create_url( $message ) ) {
			echo $telefication_service->send_notification();
		}
		die;
	}

	/**
	 * Ajax get chat id
	 *
	 * @since    1.4.0
	 */
	public function get_chat_id() {

		if ( ! isset( $_REQUEST['bot_token'] ) || empty( $_REQUEST['bot_token'] ) ) {
			_e( 'Please enter bot token', 'telefication' );
			die;
		}

		$telefication_service                     = new Telefication_Service( $this->options );
		$telefication_service->telegram_bot_token = $_REQUEST['bot_token'];

		echo empty( $telefication_service->get_chat_id() ) ? __( 'Please send something to your Bot (e.g. say hello :) ) so Telefication can get your id.',
			'telefication' ) : $telefication_service->get_chat_id();
		die;
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {

		$this->loader->run();
	}

}